#![feature(test)]
extern crate test;

use std::fs::File;
use std::io;
use std::io::BufReader;
use test::{black_box, Bencher};
use utf8_bufread::BufRead;

// This will be used in benches to determine the impact of cloning
// It seems to be ~1% to ~5% slower when cloning (cost is greater for larger buffer sizes), but
// would need more testing on other hardware and platforms
pub fn read_utf8_no_clone<R>(r: &mut R) -> io::Result<usize>
where
    R: BufRead + ?Sized,
{
    r.with_utf8_chunk(|s| {
        let _: &str = black_box(s);
    })
}

#[bench]
fn wikipedia_arabic_usa_128_clone(b: &mut Bencher) {
    b.iter(|| {
        let mut r = BufReader::with_capacity(
            128,
            File::open("./test_assets/wikipedia_arabic_USA.html").unwrap(),
        );
        let mut buf = String::new();

        loop {
            match r.read_utf8(&mut buf) {
                Ok(0) => break,
                Ok(_) => {
                    buf.clear();
                }
                Err(e) => panic!("{}", e),
            }
        }
    })
}

#[bench]
fn wikipedia_arabic_usa_8096_clone(b: &mut Bencher) {
    b.iter(|| {
        let mut r = BufReader::with_capacity(
            8096,
            File::open("./test_assets/wikipedia_arabic_USA.html").unwrap(),
        );
        let mut buf = String::new();

        loop {
            match r.read_utf8(&mut buf) {
                Ok(0) => break,
                Ok(_) => {
                    buf.clear();
                }
                Err(e) => panic!("{}", e),
            }
        }
    })
}
#[bench]
fn wikipedia_arabic_usa_128_no_clone(b: &mut Bencher) {
    b.iter(|| {
        let mut r = BufReader::with_capacity(
            128,
            File::open("./test_assets/wikipedia_arabic_USA.html").unwrap(),
        );
        let mut buf = String::new();

        loop {
            match read_utf8_no_clone(&mut r) {
                Ok(0) => break,
                Ok(_) => {
                    buf.clear();
                }
                Err(e) => panic!("{}", e),
            }
        }
    })
}

#[bench]
fn wikipedia_arabic_usa_8096_no_clone(b: &mut Bencher) {
    b.iter(|| {
        let mut r = BufReader::with_capacity(
            8096,
            File::open("./test_assets/wikipedia_arabic_USA.html").unwrap(),
        );
        let mut buf = String::new();

        loop {
            match read_utf8_no_clone(&mut r) {
                Ok(0) => break,
                Ok(_) => {
                    buf.clear();
                }
                Err(e) => panic!("{}", e),
            }
        }
    })
}
