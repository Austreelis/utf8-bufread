use std::io::{BufReader, Cursor};
use utf8_bufread::BufRead;

#[test]
#[should_panic]
fn into_inner_with_leftovers() {
    let e = BufReader::with_capacity(2, [240u8, 21, 21, 21].as_ref())
        .read_str()
        .unwrap_err();
    // We have leftovers
    assert!(!e.leftovers().is_empty());
    let _e = e.into_inner();
}

#[test]
// Should not panic
fn into_inner_no_leftovers() {
    let e = Cursor::new([240, 21, 21, 21]).read_str().unwrap_err();
    // We don't have leftovers
    assert!(e.leftovers().is_empty());
    let _e = e.into_inner();
}

#[test]
// Should not panic
fn into_inner_lossy_with_leftovers() {
    let e = BufReader::with_capacity(2, [240, 21, 21, 21].as_ref())
        .read_str()
        .unwrap_err();
    // We have leftovers
    assert!(!e.leftovers().is_empty());
    let _e = e.into_inner_lossy();
}

#[test]
// Should not panic
fn into_inner_lossy_no_leftovers() {
    let e = Cursor::new([240, 21, 21, 21]).read_str().unwrap_err();
    // We don't have leftovers
    assert!(e.leftovers().is_empty());
    let _e = e.into_inner_lossy();
}

#[test]
fn into_inner_checked_with_leftovers() {
    let e = BufReader::with_capacity(2, [240, 21, 21, 21].as_ref())
        .read_str()
        .unwrap_err();
    // We have leftovers
    assert!(!e.leftovers().is_empty());
    assert!(e.into_inner_checked().is_err());
}

#[test]
fn into_inner_checked_no_leftovers() {
    let e = Cursor::new([240, 21, 21, 21]).read_str().unwrap_err();
    // We don't have leftovers
    assert!(e.leftovers().is_empty());
    assert!(e.into_inner_checked().is_ok());
}
