{
  inputs = {
    nixpkgs.url = "nixpkgs";
    nixCargoIntegration = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { ... } @ inputs:
    let
      crate = {
        root = ./.;
        buildPlatform = "crate2nix";
        defaultOutputs.package = "utf8-bufread";
      };

      crate.overrides.pkgs = common: prev: { config.allowUnfree = true; };
      crate.overrides.shell = common: prev: common.lib.mkMerge [ prev {
        # Add a low-prio rustup. Avoids conflict with other packages like
        # clippy in the devshell env
        # Some IDEs seems to have a hard time working with rust-overlay's
        # toolchains
        # TODO: Investigate above comment
        devshell.packages = [ (common.lib.lowPrio common.pkgs.rustup) ];
      } ];

    in
    inputs.nixCargoIntegration.lib.makeOutputs crate;
}
